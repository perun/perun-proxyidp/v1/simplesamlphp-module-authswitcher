<?php

declare(strict_types=1);

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Module\authswitcher\AuthnContextHelper;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();
$t = new Template($config, AuthnContextHelper::WEAK_PASSWORD_TPL_URL);

if (empty($_GET['stateId'])) {
    exit;
}

$t->data[AuthnContextHelper::CHANGE_WEAK_PASSWORD_URL_ENABLED] = false;
$state = State::loadState($_GET['stateId'], 'authswitcher:authncontexthelper', true);
$change_password_urls = $state[AuthnContextHelper::PARAM_CHANGE_WEAK_PASSWORD_URL];
if (!empty($change_password_urls)) {
    $t->includeInlineTranslation(AuthnContextHelper::PARAM_CHANGE_WEAK_PASSWORD_URL, $change_password_urls);
    $t->data[AuthnContextHelper::CHANGE_WEAK_PASSWORD_URL_ENABLED] = true;
}

$t->show();

<?php

declare(strict_types=1);

use SimpleSAML\Module\authswitcher\AuthnContextHelper;

$this->includeAtTemplateBase('includes/header.php');
?>

<div class="row">
    <div>
        <p><?php echo $this->t('{authswitcher:password:change_weak_password_text}'); ?></p>
        <?php if (!empty($this->data[AuthnContextHelper::CHANGE_WEAK_PASSWORD_URL_ENABLED])) { ?>
            <a class="btn btn-lg btn-block btn-primary"
               href="<?php echo $this->t(AuthnContextHelper::PARAM_CHANGE_WEAK_PASSWORD_URL); ?>">
                <?php echo $this->t('{authswitcher:password:change_weak_password_button}'); ?>
            </a>
        <?php } ?>

    </div>
</div>

<?php
$this->includeAtTemplateBase('includes/footer.php');
?>
